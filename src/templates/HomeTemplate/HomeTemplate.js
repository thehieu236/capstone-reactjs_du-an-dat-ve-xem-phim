import {Route} from "react-router-dom";
import {Fragment} from "react";

export const HomeTemplate = (props) => {
    const {Component, ...restProps} = props

    return <Route {...restProps} render={(propsRoute) => {

        return <Fragment>
            <h1 className="h-10 ">Đây là header homepage</h1>
            <Component {...propsRoute}/>

            <footer className="h-10">
                Đây là footer homepag
            </footer>
        </Fragment>
    }}/>
}