import * as React from 'react';
import {movieServices} from "../../services/movieServices";
import {useParams} from "react-router-dom";
import {useState, useEffect} from "react";
import Button from "../../Components/Button/Button";
import moment from "moment/moment";
import Box from '@mui/joy/Box';
import CircularProgress from '@mui/joy/CircularProgress';
import {CssVarsProvider, StyledEngineProvider} from "@mui/joy";
import MoviesTabDetail from "./MoviesTabDetail/MoviesTabDetail";

const DetailPage = () => {
    const {moviesId} = useParams()

    const [moviesDetailArr, setMoviesDetailArr] = useState([]);
    useEffect(() => {
        movieServices
            .getMoviesDetail(`${moviesId}`)
            .then((res) => {
                setMoviesDetailArr(res.data?.content || [])
            })
            .catch((err) => {
                console.log("err", err);
            });

    }, [moviesId]);

    const {hinhAnh, ngayKhoiChieu, tenPhim, moTa, danhGia} = moviesDetailArr

    return (
        <div className={"container py-5 "}>
            <div className={"flex items-center"}>
                <img src={hinhAnh} alt="" className={"w-[300px] h-400px] m-3"}/>
                <div className={"w-[500px]"}>
                    <div className={"rate"}>
                        <StyledEngineProvider injectFirst>
                            <CssVarsProvider>
                                <CircularProgressChildren val={danhGia}/>
                            </CssVarsProvider>
                        </StyledEngineProvider>
                    </div>

                    {moment(ngayKhoiChieu).format("DD/MM/YYYY - hh:mm A")}
                    <h1 className={"font-bold my-3 text-2xl"}>{tenPhim}</h1>
                    <span>{moTa}</span>
                    <br/>


                    <Button
                        className="bg-red-500 px-5 py-2 rounded  text-white shadow  transition hover:text-white mt-5 w-full">Mua
                        vé</Button>
                </div>


            </div>

            <MoviesTabDetail moviesId={moviesId}/>
        </div>
    );
};

function CircularProgressChildren(props) {
    return (

        <Box sx={{display: 'flex', flexDirection: 'column', gap: 2, alignItems: 'center', flexWrap: 'wrap',}}>

            <CircularProgress size="lg" determinate value={props.val * 10}>
                <span>{props.val} / 10</span>
            </CircularProgress>
            <div className={"block rate text-amber-400"}>
                <i className="fa fa-splotch"></i>
                <i className="fa fa-splotch"></i>
                <i className="fa fa-splotch"></i>
                <i className="fa fa-splotch"></i>
                <i className="fa fa-splotch"></i>
            </div>
        </Box>
    );
}


export default DetailPage;
