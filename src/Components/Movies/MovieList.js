import React from 'react';
import MovieItems from "./MovieItems";
import "swiper/css/grid";
import "swiper/css/pagination";
import Slider from 'react-slick';

const settings = {
    // centerMode: true,
    // centerPadding: '10px',
    slidesToShow: 4,
    // slidesToScroll: 1,
    slidesPerRow: 2,
    // arrows: true,admin0031
    dots: true,
    responsive: [
        {
            breakpoint: 1280,
            settings: {
                arrows: false,
                slidesToShow: 3,
            },
        },
        {
            breakpoint: 1204,
            settings: {
                arrows: false,
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 1,
            },
        },
    ],
};

function MovieList(data) {
    return (
        <div id={"lichChieu"} className={"movie-list py-5"}>
            <Slider {...settings}
            >
                {data.moviesArr.map((item) => (
                    <MovieItems data={item}/>
                ))}
            </Slider>

        </div>

    );
}

export default MovieList;