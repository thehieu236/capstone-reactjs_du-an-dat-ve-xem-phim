import React from "react";
import {Button, Checkbox, Form, Input} from "antd";
import Lottie from "lottie-react";
import bg_animate from "../../Assets/32615-cinema.json";
import {NavLink, useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {setLoginActionService} from './../../redux/actions/userAction';

export default function LoginPage() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    const onFinish = (values) => {
        let onNavigate = () => {
            setTimeout(() => {
                navigate("/");
            }, 1000);
        };
        dispatch(setLoginActionService(values, onNavigate));
    };
    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };

    return (
        <div className="container mx-auto flex align-center row p-3">
            <div className="col-12 col-md-6 h-full p-10 flex justify-center">
                <Lottie animationData={bg_animate} loop={true}/>
            </div>
            <div className="col-12 col-md-6 h-full justify-center flex my-auto">
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <h1 className="text-center text-xl mb-5 font-serif font-bold">
                        Login your account
                    </h1>
                    <Form.Item
                        label="Username"
                        name="taiKhoan"
                        rules={[
                            {
                                required: true,
                                message: "Please input your username!",
                            },
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Password"
                        name="matKhau"
                        rules={[
                            {
                                required: true,
                                message: "Please input your password!",
                            },
                        ]}
                    >
                        <Input.Password/>
                    </Form.Item>
                    <div className={"text-center"}>
                        <Button
                            className="bg-red-500 w-full mb-3 text-white hover:bg-gray-400 border-none mx-auto"
                            htmlType="submit"
                        >
                            Submit
                        </Button>

                        <NavLink to="/register">
                            <button
                                className="bg-yellow-500 text-white rounded h-8 w-full pr-4 pl-4 hover:bg-gray-400 border-none mx-auto">
                                Register new account now
                            </button>
                        </NavLink>
                    </div>

                </Form>
            </div>
        </div>
    );
}
