import axios from "axios";
import {https} from "./configURL";
import {BASE_URL, createConfig} from "./configURL";

export const movieServices = {
    getListMovies: () => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03`,
        //     method: 'GET',
        //     headers: createConfig(),
        // })
        return https.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03`)
    },
    getBannerMovies: () => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
        //     method: 'GET',
        //     headers: createConfig()
        // })
        return https.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`)

    },
    getMovieSchedule: () => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03`,
        //     method: "GET",
        //     headers: createConfig()
        // })
        return https.get(`${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03`)

    },
    getMovieScheduleDetail: (idMovie) => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${idMovie}`,
        //     method: "GET",
        //     headers: createConfig()
        // })
        return https.get(`${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${idMovie}`)

    },
    getMoviesDetail: (idMovie) => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${idMovie}`,
        //     method: "GET",
        //     headers: createConfig()
        // })
        return https.get(`${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${idMovie}`)

    }
};