import React from 'react';
import {useState, useEffect} from "react";
import {movieServices} from "../../../services/movieServices";
import {Tabs} from "antd";
import moment from "moment/moment";

export default function MoviesTabDetail(props) {
    const [dataMovie, setDataMovie] = useState([]);
    useEffect(() => {
        movieServices
            .getMovieScheduleDetail(props.moviesId)
            .then((res) => {
                setDataMovie(res.data.content.heThongRapChieu
                );
            })
            .catch((err) => {
                console.log(err);
            });
    }, [props.moviesId]);

    let renderHeThongRap = () => {
        return dataMovie.map((heThongRap) => {
            return {
                label: (
                    <img
                        className="w-16 h-16 object-cover"
                        src={heThongRap.logo}
                        alt=""
                    />
                ),
                key: heThongRap.maHeThongRap,
                children: (
                    <Tabs
                        tabPosition="left"
                        defaultActiveKey="1"
                        onChange={onChange}
                        items={heThongRap.cumRapChieu.map((cumRap) => {
                            return {
                                label: (
                                    <div className={"w-[900px] text-left"}>
                                        <p>{cumRap.tenCumRap}</p>

                                        <div className={"mt-3"}>{cumRap.lichChieuPhim.map((phim) => {
                                            return <span className="bg-red-500 text-white rounded p-2 m-2">
                                            {moment(phim.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm A")}
                                          </span>
                                        })}</div>
                                    </div>
                                ),
                                key: cumRap.maCumRap,

                            };
                        })}
                    />
                ),
            };

        })
    }

    const onChange = (key) => {
        console.log(key)
    };

    return (
        <div>
            <Tabs
                tabPosition="left"
                defaultActiveKey="1"
                onChange={onChange}
                items={renderHeThongRap()}
            />
        </div>
    );
}
