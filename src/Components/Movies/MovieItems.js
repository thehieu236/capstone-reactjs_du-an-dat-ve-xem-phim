import React from 'react';
import {useNavigate} from "react-router-dom";
import Button from "../Button/Button";

const MovieItems = (item) => {

    let {tenPhim, maPhim, hinhAnh, moTa, danhGia, ngayKhoiChieu} = item.data
    const navigate = useNavigate()

    return (
        <div
            className="movie-card flex flex-col rounded-lg p-3 bg-slate-800 text-white select-none border-none h-[700px] mt-3">
            <img
                src={hinhAnh}
                alt="" className="w-full h-[350px] object-cover rounded-lg mb-5"/>

            <div className="flex flex-col flex-1">
                <h3 className="text-xl font-bold mb-3">{tenPhim}</h3>
                <span className={"lineClamp mb-5"}>{moTa}</span>
                <div className="flex items-center justify-between text-sm opacity-50 mt-auto">
                    <span>{new Date(ngayKhoiChieu).getFullYear()}</span>
                    <span>{danhGia} <i className="text-yellow-700 fa fa-star"></i></span>
                </div>
                {/*<Button className={"bg-red-500 py-3 px-6 rounded-lg capitalize mt-auto w-full"}>Mua vé</Button>*/}

                <Button
                    onClick={() => navigate(`/Detail/${maPhim}`)}
                    className="bg-red-500 px-5 py-2 rounded  text-white shadow  transition hover:text-white mt-auto"
                >
                    Xem chi tiết
                </Button>
            </div>

        </div>
    );
};

export default MovieItems;
