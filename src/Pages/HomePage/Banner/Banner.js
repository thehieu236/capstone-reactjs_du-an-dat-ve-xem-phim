import React from 'react';
import {useEffect, useState} from "react";
import {movieServices} from "../../../services/movieServices";
import {Swiper, SwiperSlide} from "swiper/react";
import "swiper/css/navigation";
import {Navigation} from "swiper";
import {ButtonPlay} from "../../../Assets/ButtonPlay";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    height: 'auto',
    bgcolor: 'background.paper',
    boxShadow: 24,
};

const Banner = () => {

    const [bannerArr, setBannerArr] = useState([]);

    useEffect(() => {
        movieServices
            .getBannerMovies()
            .then((res) => {
                setBannerArr(res.data?.content || [])
            })
            .catch((err) => {
                console.log("err", err);
            });

    }, []);

    return (
        <section className={"banner h-[700px] w-full mb-10 overflow-hidden"}>
            <Swiper navigation={true} modules={[Navigation]} className="mySwiper">
                {bannerArr.length > 0 && bannerArr.map((item) => (
                    <SwiperSlide className="" key={item.maBanner}>
                        <BannerItem item={item}/>
                    </SwiperSlide>
                ))}
            </Swiper>

        </section>
    );
};


function BannerItem({item}) {
    const {hinhAnh} = item
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    return (
        <section className="w-full h-full cursor-pointer relative">
            <div
                className="overlay absolute inset-0 bg-gradient-to-t from-[rgb(0,0,0.3)] to-[rgb(0,0,0,0.3)] rounded-lg">
                <button onClick={handleOpen}><ButtonPlay/></button>

            </div>
            <img
                src={hinhAnh}
                alt=""
                className="h-full w-full object-cover object-center"
            />
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-description" sx={{mt: 2}}>
                        <iframe style={{width: '100%', height: '520px'}} src="https://www.youtube.com/embed/uqJ9u7GSaYM"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                allowFullScreen></iframe>
                    </Typography>
                </Box>
            </Modal>
        </section>)
}

export default Banner;
