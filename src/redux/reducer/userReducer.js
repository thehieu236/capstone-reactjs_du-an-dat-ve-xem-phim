import {userLocalStorageService} from '../../services/localStorageService';
import {SET_USER_INFO} from './../constant/userConstant';

const initialState = {
    userInfor: userLocalStorageService.get(),
};


export const userReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case SET_USER_INFO:
            return {...state, userInfor: payload};
        default:
            return state;
    }
}
