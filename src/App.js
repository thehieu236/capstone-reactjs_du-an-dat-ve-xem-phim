import {BrowserRouter, Route, Routes} from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import RegisterPage from "./Pages/LoginPage/RegisterPage";
import Main from "./Components/Layout/Main";
import DetailPage from "./Pages/DetailPage/DetailPage";
import 'bootstrap/dist/css/bootstrap.min.css';
import Spinner from "./Components/Spinner/Spinner"
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";

function App() {
    return (
        <BrowserRouter>
            <Spinner/>
            <Routes>
                <Route element={<Main/>}>
                    <Route path="/" element={
                        <>
                            <HomePage/>
                        </>
                    }/>
                    <Route path={"detail/:moviesId"} element={<DetailPage/>}/>
                    <Route path="/register" element={<RegisterPage/>}/>
                    <Route path="/login" element={<LoginPage/>}/>
                    <Route path={"/loading"} element={<Spinner/>}/>

                </Route>
                <Route path="/*" element={<NotFoundPage/>}/>
            </Routes>

        </BrowserRouter>
    );
}

export default App
