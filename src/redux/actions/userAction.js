import {SET_USER_INFO} from "./../constant/userConstant";
import {userService} from "./../../services/userService";
import {userLocalStorageService} from "../../services/localStorageService";
import {message} from "antd";
import {setUserInfor} from "../../redux-toolkit/slice/userSlice";
// redux thunk
export const setLoginActionService = (userData, onNavigate) => {
    // sử dụng redux thunk
    // call api xử lý user đăng nhập
    return (dispatch) => {
        userService
            .postLogin(userData)
            .then((res) => {
                // thông báo đăng nhập thành công
                message.success("Đăng nhập thành công");
                // lưu vào localStogare
                userLocalStorageService.set(res.data.content);
                // đẩy data lên redux
                dispatch(setUserInfor(res.data.content));
                onNavigate();
            })
            .catch((err) => {
                // thông báo đăng nhập thất bại
                message.error(err.response.data.content);
                console.log(err);
            });
    };
};
export const setSignUpActionService = (userData, onNavigate) => {
    // call api đăng kí user mới
    return () => {
        userService
            .postSignUp(userData)
            .then((res) => {
                console.log('res', res)
                // thông báo đăng kí thành công và chuyển hướng trang về login
                message.success("Tài khoản đã được đăng kí thành công");
                onNavigate();
            })
            .catch((err) => {
                message.error(err.response.data.content);
                console.log(err);
            });
    };
};
