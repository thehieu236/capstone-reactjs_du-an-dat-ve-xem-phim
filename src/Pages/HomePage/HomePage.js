import React from 'react';
import {useState, useEffect} from "react";
import 'swiper/css';
import {movieServices} from "../../services/movieServices";
import MovieList from "../../Components/Movies/MovieList";
import MovieTabs from "./MovieTab/MovieTab";
import Banner from "./Banner/Banner";
import DownloadApp from "./DownloadApp/DownloadApp";
import News from "./News/News";
import MenuFilter from "./MenuFilter/MenuFilter";

function HomePage() {

    const [moviesArr, setMoviesArr] = useState([]);
    useEffect(() => {
        movieServices
            .getListMovies({})
            .then((res) => {
                setMoviesArr(res.data?.content || [])
            })
            .catch((err) => {
                console.log("err", err);
            });

    }, []);

    return (
        <>
            <Banner/>
            <div className={"container mx-auto py-5"}>
                <div className={"flex justify-center items-center -mt-[100px] z-20"}>
                    <MenuFilter movieArr={moviesArr}/>
                </div>
                <MovieList moviesArr={moviesArr}/>
                <MovieTabs/>
                <News/>
            </div>
            <DownloadApp/>
        </>


    );
}


export default HomePage;