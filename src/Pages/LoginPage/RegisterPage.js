import React, {useState} from "react";
import {setSignUpActionService} from "../../redux/actions/userAction";
import {useDispatch} from 'react-redux';
import {useNavigate} from 'react-router-dom';
import {
    Button,
    Form,
    Input,
    Select,
} from "antd";

const {Option} = Select;
const formItemLayout = {
    labelCol: {
        xs: {
            span: 16,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 16,
        },
        sm: {
            span: 8,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 16,
            offset: 0,
        },
        sm: {
            span: 8,
            offset: 8,
        },
    },
};
export default function RegisterPage() {
    let dispatch = useDispatch();
    let navigate = useNavigate();

    const [form] = Form.useForm();
    const onFinish = (values) => {
        let onNavigate = () => {
            setTimeout(() => {
                navigate("/login")
            }, 1000);
        };


        dispatch(setSignUpActionService(values, onNavigate));


    };
    return (
        <div className="flex justify-center items-center mt-[80px]">
            <div className="container bg-white h-full mt-10">
                <h1 className="font-serif mb-2">Register new account</h1>
                <Form
                    {...formItemLayout}
                    form={form}
                    name="register"
                    onFinish={onFinish}
                    initialValues={{
                        residence: ["zhejiang", "hangzhou", "xihu"],
                        prefix: "86",
                    }}
                    scrollToFirstError
                >
                    <Form.Item
                        name="taiKhoan"
                        label="Nickname"
                        tooltip="What do you want others to call you?"
                        rules={[
                            {
                                required: true,
                                message: "Please input your nickname!",
                                whitespace: true,
                            },
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        name="hoTen"
                        label="name"
                        rules={[
                            {
                                required: true,
                                message: "vui lòng nhập họ và tên",
                            }
                        ]}

                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        name="maKhau"
                        label="Password"
                        rules={[
                            {
                                required: true,
                                message: "Please input your password!",
                            },
                        ]}
                        hasFeedback
                    >
                        <Input.Password/>
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="E-mail"
                        rules={[
                            {
                                type: "email",
                                message: "The input is not valid E-mail!",
                            },
                            {
                                required: true,
                                message: "Please input your E-mail!",
                            },
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        name="soDt"
                        label="Phone Number"
                        rules={[
                            {
                                required: true,
                                message: "Please input your phone number!",
                            },
                            {
                                pattern: new RegExp(/^[0-9]+$/),
                                message: "số điện thoại phải là số"
                            }
                        ]}
                    >
                        <Input
                            maxLength={10}
                            style={{
                                width: "100%",
                            }}
                        />
                    </Form.Item>
                    <Form.Item
                        name="maNhom"
                        label="Mã nhóm"
                        rules={[
                            {
                                required: true,
                                message: "vui lòng chọn mã nhóm",
                            },
                        ]}
                    >
                        <Select style={{width: "50%"}}>
                            <Option value="GP00">GP00</Option>
                            <Option value="GP01">GP01</Option>
                            <Option value="GP02">GP02</Option>
                            <Option value="GP03">GP03</Option>
                            <Option value="GP04">GP04</Option>
                            <Option value="GP05">GP05</Option>
                            <Option value="GP06">GP06</Option>
                            <Option value="GP07">GP07</Option>
                            <Option value="GP08">GP08</Option>
                            <Option value="GP09">GP09</Option>
                            <Option value="GP10">GP10</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button
                            className="bg-red-500 text-white hover:bg-gray-400 border-none"
                            type="primary"
                            htmlType="submit"
                        >
                            Register
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
