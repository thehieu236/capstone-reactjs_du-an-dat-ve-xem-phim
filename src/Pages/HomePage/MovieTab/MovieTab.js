import React from 'react';
import {useState, useEffect} from "react";
import {movieServices} from "../../../services/movieServices";
import {Tabs} from "antd";
import MovieTabItem from "./MovieTabItem";


export default function MovieTabs() {
    const [dataMovie, setDataMovie] = useState([]);
    useEffect(() => {
        movieServices
            .getMovieSchedule()
            .then((res) => {
                setDataMovie(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    let renderHeThongRap = () => {
        return dataMovie.map((heThongRap) => {
            return {
                label: (
                    <img
                        className="w-16 h-16 object-cover"
                        src={heThongRap.logo}
                        alt=""
                    />
                ),
                key: heThongRap.maHeThongRap,
                children: (
                    <Tabs
                        tabPosition={window.innerWidth < 768 ? "bottom" : "left"}
                        defaultActiveKey="1"
                        onChange={onChange}
                        items={heThongRap.lstCumRap.map((cumRap) => {
                            return {
                                label: (
                                    <div>
                                        <p>{cumRap.tenCumRap}</p>
                                    </div>
                                ),
                                key: cumRap.maCumRap,
                                children: cumRap.danhSachPhim.map((phim) => {
                                    return <MovieTabItem movie={phim}/>;
                                }),
                            };
                        })}
                    />
                ),
            };
        });
    };

    const onChange = (key) => {
        console.log(key);
    };

    return (
        <div id={"cumRap"}>
            <Tabs
                tabPosition="left"
                defaultActiveKey="1"
                onChange={onChange}
                items={renderHeThongRap()}
            />
        </div>
    );
}
