import axios from "axios";
import {BASE_URL, createConfig, https} from './configURL';

export const userService = {
    postLogin: (dataUser) => {
        // return axios({
        //     url:`${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
        //     method:"POST",
        //     data:dataUser,
        //     headers: createConfig(),
        // });
        return https.post(`${BASE_URL}/api/QuanLyNguoiDung/DangNhap`, dataUser)
    },
    postSignUp: (dataUser) => {
        // return axios({
        //     url:`${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
        //     method:"POST",
        //     data:dataUser,
        //     headers:createConfig(),
        // });
        return https.post(`${BASE_URL}/api/QuanLyNguoiDung/DangKy`, dataUser)
    },
};