import React from 'react';
import '../../../Assets/img/backappb.jpg'
import './downloadApp.css'
import Button from "../../../Components/Button/Button";

const DownloadApp = () => {
    return (
        <section id={"app"} className={"downloadApp"}>
            <div className="container md:flex justify-center block text-center">
                <div className="item text-white p-3">
                    <h1 className={"text-4xl mb-2"}>Ứng dụng tiện lợi dành cho <br/> người yêu điện ảnh</h1>
                    <p>Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp dẫn.</p>
                    <Button
                        className={"py-3 px-6 rounded-lg capitalize  bg-red-500 rounded  text-white shadow  transition hover:text-white mt-3 "}>APP
                        MIỄN PHÍ -
                        TẢI VỀ NGAY</Button>
                    <span className={"block mt-4"}>TIX có hai phiên bản <a
                        href="src/Pages/HomePage/DownloadApp#">IOS</a> & <a
                        href="src/Pages/HomePage/DownloadApp#">Android</a></span>
                </div>
                <div className="appPhone item w-[500px] flex justify-center mx-auto">
                    <img className={"h-[300px] absolute rounded-xl"}
                         src={require("../../../Assets/img/banner-slider-phaone.jpg")}
                         alt=""/>
                    <img className={"h-[300px]"} src={require("../../../Assets/img/bg-phone.png")} alt=""/>

                </div>
            </div>


        </section>
    );
};

export default DownloadApp;
