import React from 'react';

const Button = ({onClick, className, full = false, type = "button", children}) => {

    return (
        <button
            type={type}
            onClick={onClick}
            className={`py-3 px-6 rounded-lg capitalize ${full ? 'w-full' : ""} ${className}`}>
            {children}
        </button>
    );
};
export default Button;