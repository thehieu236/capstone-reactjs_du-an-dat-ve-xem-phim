import React from 'react';
import {useSelector} from 'react-redux';
import {RingLoader} from 'react-spinners';

export default function Spinner() {
    let {isLoading} = useSelector((state) => state.spinnerSlice)
    console.log('isLoading', isLoading)
    return isLoading ? (
        <div className='fixed w-screen h-screen top-0 left-0 z-50 bg-slate-500 flex items-center justify-center'>
            <RingLoader size={300} color="rgba(54, 215, 183, 1)"/>
        </div>
    ) : (<></>);
};
