import React, {useEffect, useState} from 'react';
import Button from "../../../Components/Button/Button";
import {movieServices} from "../../../services/movieServices";
import {Select} from 'antd';
import moment from "moment";

const MenuFilter = ({movieArr}) => {

    const [value, setValue] = useState();
    const [dataMovie, setDataMovie] = useState([]);
    useEffect(() => {
        movieServices
            .getMovieScheduleDetail(value)
            .then((res) => {
                setDataMovie(res.data.content.heThongRapChieu);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [value]);

    function err() {
        return dataMovie.map((heThongRap) => {
            return heThongRap.cumRapChieu
        }).flat()
    }
    
    return (
        <div
            className="flex justify-center max-md:flex-wrap items-center w-full md:w-2/3 -mt-[30px] py-4 px-3 shadow rounded-lg bg-white z-10 ">
            <Select
                defaultValue="Phim"
                className={"w-2/3 mr-2 mt-3"}
                onChange={(value) => setValue(value)}
                options={
                    movieArr.map((item) => (
                        {
                            value: item.maPhim,
                            label: item.tenPhim,
                        }
                    ))
                }
            />
            <Select
                defaultValue="Rạp"
                className={"w-2/3 mr-2 mt-3"}
                onChange={(value) => setValue(value)}
                options={
                    err().flatMap((item) => (
                        {
                            value: item.tenCumRap,
                            label: item.tenCumRap,
                        }
                    ))
                }

            />
            <Select
                defaultValue="Ngày Giờ Chiếu"
                className={"w-2/3 mr-2 mt-3"}
                onChange={(value) => setValue(value)}
                options={
                    err().flatMap(item => (item.lichChieuPhim.flatMap((phim) => (
                        {
                            value: phim.ngayChieuGioChieu,
                            label: moment(phim.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm A"),
                        }

                    ))))

                }
            />

            <Button
                className="w-1/3 bg-red-500 px-5 py-2 rounded text-white shadow transition hover:text-white mt-3">
                Mua vé
            </Button>
        </div>
    );
};

export default MenuFilter;
