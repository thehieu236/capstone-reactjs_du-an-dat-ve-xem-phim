import React from "react";
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import Button from "../../Button/Button";
import {userLocalService} from "../../../services/userLocalService";

export default function UserNav() {
    let user = useSelector((state) => {
        return state.userSlice.userInfor;
    });
    let handleLogout = () => {

        userLocalService.remove();
        window.location.href = "/login";
    };

    const renderContent = () => {
        if (user) {
            return (
                <>
                    <span>{user.hoTen}</span>
                    <Button
                        onClick={handleLogout}
                        className={"border-2 mr-2"}>
                        <i className="fa fa-user mr-2"></i>
                        Đăng xuất
                    </Button>

                </>
            );
        } else {
            return (
                <>
                    <NavLink to="/login">
                        <Button className={"border-2 mr-2"}>
                            <i className="fa fa-user mr-2"></i>
                            Đăng nhập
                        </Button>
                    </NavLink>
                    <NavLink to="/register">
                        <Button className={"border-2"}>
                            <i className="fa fa-user-edit mr-2"></i>
                            Đăng ký
                        </Button>
                    </NavLink>
                </>
            );
        }
    };

    return <div className="space-x-5">{renderContent()}</div>;
}
// admin 005 admin0031
// abc123 123456111
