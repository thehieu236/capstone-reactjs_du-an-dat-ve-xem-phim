import React from "react";
import {useState} from "react";

const News = ({color}) => {
    const [openTab, setOpenTab] = useState(1);
    return (
        <>
            <div id={"info"} className="flex flex-wrap mt-4 transition-all">
                <div className="w-full">
                    <ul className="flex mb-0 list-none justify-center flex-wrap pt-3 pb-4 flex-row"
                        role="tablist">
                        <li className="flex justify-center hover:text-red-500 py-4">
                            <a className={"font-bold uppercase px-5 py-3 text-xl block leading-normal hover:no-underline hover:text-red-500 " +
                                (openTab === 1
                                    ? "text-red-500 border-b-4 border-b-red-500 bg-" + color + "-600"
                                    : "text-" + color + "-600 bg-white")
                            }
                               onClick={e => {
                                   e.preventDefault();
                                   setOpenTab(1);
                               }}
                               data-toggle="tab"
                               href="src/Pages/HomePage/News#link1"
                               role="tablist"
                            >
                                Điện Ảnh 24h
                            </a>
                        </li>

                        <li className="flex justify-center hover:text-red-500 py-4">
                            <a
                                className={
                                    "font-bold uppercase px-5 py-3 text-xl block leading-normal hover:no-underline hover:text-red-500 " +
                                    (openTab === 2
                                        ? "text-red-500 border-b-4 border-b-red-500 bg-" + color + "-600"
                                        : "text-" + color + "-600 bg-white")
                                }
                                onClick={e => {
                                    e.preventDefault();
                                    setOpenTab(2);
                                }}
                                data-toggle="tab"
                                href="src/Pages/HomePage/News#link2"
                                role="tablist"
                            >
                                Review
                            </a>
                        </li>

                        <li className="flex justify-center hover:text-red-500 py-4">
                            <a
                                className={
                                    "font-bold uppercase px-5 py-3 text-xl block leading-normal hover:no-underline hover:text-red-500 " +
                                    (openTab === 3
                                        ? "text-red-500 border-b-4 border-b-red-500 bg-" + color + "-600"
                                        : "text-" + color + "-600 bg-white")
                                }
                                onClick={e => {
                                    e.preventDefault();
                                    setOpenTab(3);
                                }}
                                data-toggle="tab"
                                href="src/Pages/HomePage/News#link3"
                                role="tablist"
                            >
                                Khuyền mãi
                            </a>
                        </li>

                    </ul>
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
                        <div className="px-4 py-5 flex-auto">
                            <div className="tab-content tab-space">
                                <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                                    <div className="container grid grid-cols-6 gap-5">
                                        <div className="col-span-6 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/goc-dien-anh/7943-tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam"
                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2020/07/tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam-15959320391357.png"
                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/goc-dien-anh/7943-tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam"
                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    TENET công bố ngày khởi chiếu chính thức tại Việt Nam
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Đêm qua theo giờ Việt Nam, hãng phim Warner Bros. đưa ra thông báo chính
                                                thức về ngày khởi chiếu cho bom tấn TENET tại các thị trường bên ngoài
                                                Bắc
                                                Mỹ, trong đó có Việt Nam.
                                            </p>
                                        </div>
                                        <div className="col-span-6 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/goc-dien-anh/7941-khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan"
                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2020/07/khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan-15943683481617.jpg"
                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/goc-dien-anh/7941-khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan"
                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    Khi phụ nữ không còn ở thế trốn chạy của nạn nhân
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Là bộ phim tâm lý li kỳ với chủ đề tội phạm, Bằng Chứng Vô Hình mang đến
                                                một góc nhìn mới về hình ảnh những người phụ nữ thời hiện đại. Điều đó
                                                được thể hiện qua câu chuyện về hai người phụ nữ cùng hợp sức để vạch
                                                mặt
                                                tên tội phạm có sở thích bệnh hoạn với phụ nữ.
                                            </p>
                                        </div>
                                        <div className="col-span-6 lg:col-span-2 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/goc-dien-anh/7940-gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland"
                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2020/07/gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland-15937528932506.png"
                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/goc-dien-anh/7940-gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland"
                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    Gerard Butler cùng bồ cũ Deadpool tham gia Greenland
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Bộ phim hành động mang đề tài tận thế Greenland: Thảm Họa Thiên Thạch
                                                đến
                                                từ nhà sản xuất của loạt phim John Wick đã tung ra trailer đầu tiên, hé
                                                lộ
                                                nội dung cốt truyện, dàn diễn viên, cùng hàng loạt đại cảnh cháy nổ
                                                hoành
                                                tráng.
                                            </p>
                                        </div>
                                        <div className="col-span-6 lg:col-span-2 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/goc-dien-anh/7939-dien-vien-dac-biet-cua-bang-chung-vo-hinh"
                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2020/07/dien-vien-dac-biet-cua-bang-chung-vo-hinh-15937518743844.png"
                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/goc-dien-anh/7939-dien-vien-dac-biet-cua-bang-chung-vo-hinh"
                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    Diễn viên đặc biệt của Bằng Chứng Vô Hình
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Bằng Chứng Vô Hình tiết lộ thêm với khán giả một diễn viên vô cùng đặc
                                                biệt, đi diễn như đi chơi và không hề nghe theo sự chỉ đạo của đạo diễn
                                                Trịnh Đình Lê Minh. Đó chính là chú chó Ben – trợ thủ đắc lực của cô gái
                                                mù Thu (Phương Anh Đào).
                                            </p>
                                        </div>
                                        <div className="grid col-span-6 lg:col-span-2 h-full">
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/goc-dien-anh/7938-pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep"
                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2020/07/pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep-15937498464029.png"
                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/goc-dien-anh/7938-pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep"
                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        Pee Nak 2 - Vạn kiếp thiên thu, đi tu không hết nghiệp!
                                                    </p>
                                                </a>
                                            </div>
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/goc-dien-anh/7937-loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7"
                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2020/07/loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7-15937470779379.png"
                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/goc-dien-anh/7937-loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7"
                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        Loạt phim kinh dị không thể bỏ lỡ trong tháng 7!
                                                    </p>
                                                </a>
                                            </div>
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/goc-dien-anh/7936-rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de"
                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2020/06/rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de-15929959532579.jpg"
                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/goc-dien-anh/7936-rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de"
                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        RÒM tung trailer hé lộ cuộc sống của dân chơi số đề
                                                    </p>
                                                </a>
                                            </div>

                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/goc-dien-anh/7935-antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them"
                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2020/06/antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them-15929866818923.jpg"
                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className=""
                                                    target="_blank"
                                                    href="https://tix.vn/goc-dien-anh/7935-antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them"
                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1 mr-2.5">
                                                        Antebellum - Trailer cuối cùng không hé lộ bất cứ thông tin gì
                                                        thêm
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={openTab === 2 ? "block" : "hidden"} id="link2">
                                    <div className="container grid grid-cols-6 gap-5">
                                        <div className="col-span-6 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/review/7876-review-nang-3-loi-hua-cua-cha-cau-chuyen-tinh-than-cam-dong-cua-kha-nhu-va-kieu-minh-tuan"
                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2020/03/review-nang-3-loi-hua-cua-cha-cau-chuyen-tinh-than-cam-dong-cua-kha-nhu-va-kieu-minh-tuan-15834049872311.jpg"

                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/review/7876-review-nang-3-loi-hua-cua-cha-cau-chuyen-tinh-than-cam-dong-cua-kha-nhu-va-kieu-minh-tuan"

                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    [Review] Nắng 3: Lời Hứa Của Cha - Câu chuyện tình thân cảm động của
                                                    Khả
                                                    Như và Kiều Minh Tuấn
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Như hai phần phim trước, Nắng 3 tiếp tục mang đến câu chuyện tình cha,
                                                mẹ
                                                - con đầy nước mắt của bộ ba nhân vật chính.
                                            </p>
                                        </div>
                                        <div className="col-span-6 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/review/7871-review-onward-khi-phep-thuat-manh-me-nhat-chinh-la-tinh-than"
                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2020/03/review-onward-khi-phep-thuat-manh-me-nhat-chinh-la-tinh-than-15832047938817.jpg"

                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/review/7871-review-onward-khi-phep-thuat-manh-me-nhat-chinh-la-tinh-than"

                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    [Review] Onward - Khi phép thuật mạnh mẽ nhất chính là tình thân
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Tác phẩm mới nhất của Pixar tiếp tục là câu chuyện hài hước và cảm xúc
                                                về
                                                tình cảm gia đình.
                                            </p>
                                        </div>
                                        <div className="col-span-6 lg:col-span-2 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/review/7868-review-ke-vo-hinh-con-gi-dang-so-hon-ke-giet-nguoi-benh-hoan-vo-hinh"

                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2020/02/review-ke-vo-hinh-con-gi-dang-so-hon-ke-giet-nguoi-benh-hoan-vo-hinh-15828835353362.jpg"

                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/review/7868-review-ke-vo-hinh-con-gi-dang-so-hon-ke-giet-nguoi-benh-hoan-vo-hinh"

                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    [Review] Kẻ Vô Hình - Còn gì đáng sợ hơn kẻ giết người bệnh hoạn vô
                                                    hình?
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Phiên bản hiện đại của The Invisible Man là một trong những phim kinh dị
                                                xuất sắc nhất năm nay.
                                            </p>
                                        </div>
                                        <div className="col-span-6 lg:col-span-2 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/review/7861-review-cau-be-ma-2-ban-trai-cua-be-beo-la-day-chu-dau-xa"
                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2020/02/review-cau-be-ma-2-ban-trai-cua-be-beo-la-day-chu-dau-xa-15823608583110.jpg"

                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/review/7861-review-cau-be-ma-2-ban-trai-cua-be-beo-la-day-chu-dau-xa"

                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    [Review] Cậu Bé Ma 2 - Bạn trai của 'bé Beo' là đây chứ đâu xa
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Brahms: The Boy II có những màn hù dọa ấn tượng nhưng cái kết lại không
                                                tương xứng với phần mở đầu hứa hẹn.
                                            </p>
                                        </div>
                                        <div className="grid col-span-6 lg:col-span-2 h-full">
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/review/7859-review-nhim-sonic-cuoi-tha-ga-cung-chang-nhim-sieu-thanh-lay-loi"

                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2020/02/review-nhim-sonic-cuoi-tha-ga-cung-chang-nhim-sieu-thanh-lay-loi-15821907793369.jpg"

                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/review/7859-review-nhim-sonic-cuoi-tha-ga-cung-chang-nhim-sieu-thanh-lay-loi"

                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        Pee Nak 2 - Vạn kiếp thiên thu, đi tu không hết nghiệp!
                                                    </p>
                                                </a>
                                            </div>
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/goc-dien-anh/7937-loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7"
                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2020/07/loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7-15937470779379.png"
                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/review/7859-review-nhim-sonic-cuoi-tha-ga-cung-chang-nhim-sieu-thanh-lay-loi"

                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        [Review] Nhím Sonic - Cười thả ga cùng chàng nhím siêu thanh lầy
                                                        lội
                                                    </p>
                                                </a>
                                            </div>
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/review/7858-review-thang-nam-hanh-phuc-ta-tung-co-buong-bo-chua-bao-gio-la-viec-de-dang"

                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2020/02/review-thang-nam-hanh-phuc-ta-tung-co-buong-bo-chua-bao-gio-la-viec-de-dang-15817967038683.jpg"

                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/review/7858-review-thang-nam-hanh-phuc-ta-tung-co-buong-bo-chua-bao-gio-la-viec-de-dang"

                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        [Review] Tháng Năm Hạnh Phúc Ta Từng Có - Buông bỏ chưa bao giờ
                                                        là
                                                        việc dễ dàng
                                                    </p>
                                                </a>
                                            </div>

                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/review/7857-review-sac-dep-doi-tra-huong-giang-ke-chuyen-doi-minh-qua-phim-anh"

                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2020/02/review-sac-dep-doi-tra-huong-giang-ke-chuyen-doi-minh-qua-phim-anh-15817958389162.jpg"

                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className=""
                                                    target="_blank"
                                                    href="https://tix.vn/review/7857-review-sac-dep-doi-tra-huong-giang-ke-chuyen-doi-minh-qua-phim-anh"

                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1 mr-2.5">
                                                        [Review] Sắc Đẹp Dối Trá - Hương Giang kể chuyện đời mình qua
                                                        phim ảnh
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={openTab === 3 ? "block" : "hidden"} id="link3">
                                    <div className="container grid grid-cols-6 gap-5">
                                        <div className="col-span-6 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/khuyen-mai/7790-123phim-nhap-ma-bkt-giam-ngay-20k-khi-dat-ve-bac-kim-thang"
                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2019/10/123phim-nhap-ma-bkt-giam-ngay-20k-khi-dat-ve-bac-kim-thang-15712976725554.jpg"

                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/khuyen-mai/7790-123phim-nhap-ma-bkt-giam-ngay-20k-khi-dat-ve-bac-kim-thang"

                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    [123Phim] NHẬP MÃ 'BKT' - Giảm ngay 20k khi đặt vé Bắc Kim Thang
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                123Phim đồng hành cùng phim Việt - Giảm ngay 20k mỗi giao dịch khi đặt
                                                vé
                                                Bắc Kim Thang trên ứng dụng 123Phim.
                                            </p>
                                        </div>
                                        <div className="col-span-6 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/khuyen-mai/7774-sinh-nhat-mega-gs"

                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2019/08/sinh-nhat-mega-gs-15663933683466.jpg"

                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/khuyen-mai/7774-sinh-nhat-mega-gs"

                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    Sinh Nhật Mega GS
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Đến hẹn lại lên, vậy là một năm nữa đã trôi qua và chúng ta lại đến
                                                tháng
                                                8, tháng sinh nhật của Mega GS Cinemas.
                                            </p>
                                        </div>
                                        <div className="col-span-6 lg:col-span-2 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/khuyen-mai/7741-123phim-tixshop-tro-lai-qua-xin-hon-xua"

                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2019/05/123phim-tixshop-tro-lai-qua-xin-hon-xua-15583511037699.jpg"

                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/khuyen-mai/7741-123phim-tixshop-tro-lai-qua-xin-hon-xua"

                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    [123Phim] TixShop trở lại, quà ‘xịn’ hơn xưa
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Nhiều Tix làm gì, để tiêu vào TixShop chứ còn chờ chi.
                                            </p>
                                        </div>
                                        <div className="col-span-6 lg:col-span-2 sm:col-span-3">
                                            <div className="jss114">
                                                <a
                                                    target="_blank"
                                                    href="https://tix.vn/khuyen-mai/7732-galaxy-trang-thi-xem-phim-hay-say-qua-tang"

                                                    rel="noreferrer"
                                                >
                                                    <img
                                                        className="w-full rounded"
                                                        alt="poster"
                                                        src="https://s3img.vcdn.vn/123phim/2019/05/galaxy-trang-thi-xem-phim-hay-say-qua-tang-15572160162243.jpg"

                                                    />
                                                </a>
                                            </div>
                                            <a
                                                className="jss116"
                                                target="_blank"
                                                href="https://tix.vn/khuyen-mai/7732-galaxy-trang-thi-xem-phim-hay-say-qua-tang"

                                                rel="noreferrer"
                                            >
                                                <p className="text-2xl font-bold my-2">
                                                    [Galaxy Tràng Thi] Xem Phim Hay, Say Quà Tặng
                                                </p>
                                            </a>
                                            <p className="MuiTypography-root jss118 MuiTypography-body1">
                                                Nhân dịp khai trương Galaxy Tràng Thi, Galaxy Cinema dành tặng các Stars
                                                Hà Nội loạt phần quà siêu hấp dẫn.
                                            </p>
                                        </div>
                                        <div className="grid col-span-6 lg:col-span-2 h-full">
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/khuyen-mai/7727-mua-2-ve-cinestar-qua-zalopay-chi-1-000d-ve"

                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2019/04/mua-2-ve-cinestar-qua-zalopay-chi-1-000d-ve-15563607309238.jpg"

                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/khuyen-mai/7727-mua-2-ve-cinestar-qua-zalopay-chi-1-000d-ve"

                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        Mua 2 Vé Cinestar Qua ZaloPay Chỉ 1.000đ/vé
                                                    </p>
                                                </a>
                                            </div>
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/khuyen-mai/7723-123phim-ban-la-fan-cung-marvel"

                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2019/04/123phim-ban-la-fan-cung-marvel-15562538560772.jpg"

                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/khuyen-mai/7723-123phim-ban-la-fan-cung-marvel"

                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        [123Phim] Bạn Là Fan Cứng Marvel?
                                                    </p>
                                                </a>
                                            </div>
                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/khuyen-mai/7722-galaxy-trang-thi-trai-nghiem-bom-tan-cang-dong-cang-vui"

                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded mr-2.5"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2019/04/galaxy-trang-thi-trai-nghiem-bom-tan-cang-dong-cang-vui-15561704693167.jpg"

                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className="jss120"
                                                    target="_blank"
                                                    href="https://tix.vn/khuyen-mai/7722-galaxy-trang-thi-trai-nghiem-bom-tan-cang-dong-cang-vui"

                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                                        [Galaxy Tràng Thi] Trải Nghiệm Bom Tấn Càng Đông Càng Vui
                                                    </p>
                                                </a>
                                            </div>

                                            <div className="flex gap-y-3 mb-2">
                                                <div className="">
                                                    <a
                                                        target="_blank"
                                                        href="https://tix.vn/khuyen-mai/7716-mua-ve-bhd-star-tren-123phim-bang-zalopay-1-000d-ve"
                                                        rel="noreferrer"
                                                    >
                                                        <img
                                                            className="h-[70px] w-[70px] rounded"
                                                            alt="poster"
                                                            src="https://s3img.vcdn.vn/123phim/2019/04/mua-ve-bhd-star-tren-123phim-bang-zalopay-1-000d-ve-15547979641987.jpg"
                                                        />
                                                    </a>
                                                </div>
                                                <a
                                                    className=""
                                                    target="_blank"
                                                    href="https://tix.vn/khuyen-mai/7716-mua-ve-bhd-star-tren-123phim-bang-zalopay-1-000d-ve"

                                                    rel="noreferrer"
                                                >
                                                    <p className="MuiTypography-root jss121 MuiTypography-body1 mr-2.5">
                                                        Mua Vé BHD Star Trên 123Phim Bằng ZaloPay: 1.000đ/vé

                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default function TabsRender() {
    return (
        <>
            <News color="pink"/>
        </>
    );
}